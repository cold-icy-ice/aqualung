package io.coldicyice.aqualung.controller;

import io.coldicyice.aqualung.bean.RecordHistoryBean;
import io.coldicyice.aqualung.model.Area;
import io.coldicyice.aqualung.model.Point;
import io.coldicyice.aqualung.model.Record;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AreaCheckServlet extends HttpServlet
{
    @EJB
    private RecordHistoryBean history;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException
    {
        String result = "";
        Integer radius = null;
        Point point = new Point();
        try {
            point.setX(Double.valueOf(req.getParameter("x")));
        }
        catch (NumberFormatException e) {
            result += "Не удалось распознать параметр ‘x’.";
        }
        catch (NullPointerException e) {
            result += "Параметр ‘x’ не был указан.";
        }
        catch (IllegalArgumentException e) {
            result += e.getMessage();
        }
        try {
            point.setY(Double.valueOf(req.getParameter("y")));
        }
        catch (NumberFormatException e) {
            result += "Не удалось распознать параметр ‘y’.";
        }
        catch (NullPointerException e) {
            result += "Параметр ‘y’ не был указан.";
        }
        catch (IllegalArgumentException e) {
            result += e.getMessage();
        }
        try {
            Area area = new Area(Integer.parseInt(req.getParameter("r")));
            radius = area.getRadius();
            if (result.isEmpty()) {
                result = area.contains(point) ?
                    "Точка входит в область." :
                    "Точка находится вне области.";
            }
        }
        catch (NumberFormatException e) {
            result += "Не удалось распознать параметр ‘r’.";
        }
        catch (NullPointerException e) {
            result += "Параметр ‘r’ не был указан.";
        }

        Record record = new Record();
        record.setPoint(point);
        record.setRadius(radius);
        record.setResult(result);
        history.addRecord(record);

        req.setAttribute("history", history);
        req.getRequestDispatcher("/check.jsp").forward(req, resp);
    }
}
