package io.coldicyice.aqualung.bean;

import io.coldicyice.aqualung.model.Record;

import javax.ejb.Stateless;
import java.util.Deque;
import java.util.LinkedList;

@Stateless
public class RecordHistoryBean
{
    private static final int CAPACITY = 3;

    private Deque<Record> records;

    public RecordHistoryBean()
    {
        records = new LinkedList<>();
    }

    public Deque<Record> getRecords()
    {
        return records;
    }

    public void addRecord(Record record)
    {
        records.addFirst(record);
        while (records.size() > CAPACITY) {
            records.removeLast();
        }
    }
}
