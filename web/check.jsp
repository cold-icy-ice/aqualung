<%@ page import="io.coldicyice.aqualung.bean.RecordHistoryBean" %>
<%@ page import="io.coldicyice.aqualung.model.Record" %>
<%@ page import="java.util.Deque" %>
<%@ page import="java.util.Optional" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<% RecordHistoryBean recordHistoryBean = (RecordHistoryBean) request.getAttribute("history"); %>
<html>
<head>
    <meta charset="utf-8">
    <title>Лабораторная работа №&nbsp;2</title>
    <link rel="stylesheet" href="css/check.css">
    <link rel="shortcut icon" href="img/icon.png">
    <script type="text/javascript" src="js/draw.js"></script>
    <script type="text/javascript" src="js/drawCheck.js"></script>
    <script type="text/javascript" src="js/send.js"></script>
</head>
<body>
<table class="container">
    <%@include file="header.html" %>
    <tr id="fill">
        <td class="content" colspan="7">
            <table class="result">
                <thead>
                <tr>
                    <th class="number">x</th>
                    <th class="number">y</th>
                    <th class="number">r</th>
                    <th>Результат</th>
                </tr>
                </thead>
                <tbody>
                <%
                    Deque<Record> records = recordHistoryBean.getRecords();
                    for (Record record : records) {
                %>
                <script type="text/javascript">
                    window.addEventListener("load", function ()
                    {
                        drawPoint(canvas, <%= record.getPoint().getX() %>, <%= record.getPoint().getY() %>);
                    });
                </script>
                <tr>
                    <td class="number">
                        <%= Optional.ofNullable(record.getPoint().getX())
                                    .map(x -> String.format("%.2f", x))
                                    .orElse("—") %>
                    </td>
                    <td class="number">
                        <%= Optional.ofNullable(record.getPoint().getY())
                                    .map(x -> String.format("%.2f", x))
                                    .orElse("—") %>
                    </td>
                    <td class="number">
                        <%= Optional.ofNullable(record.getRadius())
                                    .map(Object::toString)
                                    .orElse("—") %>
                    </td>
                    <td>
                        <%= record.getResult() %>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
            <input type="hidden" id="radius"
                   value="<%= records.getFirst().getRadius() %>"/>
            <button id="back" class="crimson back" type="button"
                    onclick="window.location='index'">
                Назад
            </button>
        </td>
        <td class="sidebar" colspan="5">
            <%@include file="area.html" %>
        </td>
    </tr>
    <%@include file="footer.html" %>
</table>
</body>
</html>
