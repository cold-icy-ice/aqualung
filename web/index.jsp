<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Лабораторная работа №&nbsp;2</title>
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/form.js"></script>
    <script type="text/javascript" src="js/draw.js"></script>
    <script type="text/javascript" src="js/drawIndex.js"></script>
    <script type="text/javascript" src="js/send.js"></script>
    <link rel="shortcut icon" href="img/icon.png">
</head>
<body>
<table class="container">
    <%@include file="header.html" %>
    <tr id="fill">
        <td class="content" colspan="7">
            <form name="parameters" action="index" method="POST" onsubmit="return validateForm(this)">
                <div id="r-group">
                    <span>
                        R =
                    </span>
                    <label>
                        <input type="radio" name="r" value="1" onclick="drawGraph(canvas, radius, itmoRed);" checked>1
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </label>
                    <label>
                        <input type="radio" name="r" value="2" onclick="drawGraph(canvas, radius, itmoRed);"> 2
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </label>
                    <label>
                        <input type="radio" name="r" value="3" onclick="drawGraph(canvas, radius, itmoRed);"> 3
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </label>
                    <label>
                        <input type="radio" name="r" value="4" onclick="drawGraph(canvas, radius, itmoRed);"> 4
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </label>
                    <label>
                        <input type="radio" name="r" value="5" onclick="drawGraph(canvas, radius, itmoRed);"> 5
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </label>
                </div>
                <div id="x-group">
                    <label>
                        X =
                        <input type="number" name="x" hidden value="0"/>
                    </label>
                    <button type="button" class="x-button" value="-3">-3</button>
                    <button type="button" class="x-button" value="-2">-2</button>
                    <button type="button" class="x-button" value="-1">-1</button>
                    <button type="button" class="x-button" value="0">0</button>
                    <button type="button" class="x-button" value="1">1</button>
                    <button type="button" class="x-button" value="2">2</button>
                    <button type="button" class="x-button" value="3">3</button>
                    <button type="button" class="x-button" value="4">4</button>
                    <button type="button" class="x-button" value="5">5</button>
                </div>
                <div id="y-group">
                    <label for="y">
                        Y =
                    </label>
                    <input type="text" name="y" id="y" placeholder="(-5, +5)">
                </div>
                <button id="submit" type="submit" class="crimson submit">Проверить!</button>
            </form>
        </td>
        <td class="sidebar" colspan="5">
            <%@include file="area.html" %>
        </td>
    </tr>
    <%@include file="footer.html" %>
</table>
</body>
</html>
