function sendValues(x, y, r) {
    let form = document.createElement('form');
    form.method = "POST";
    form.action = "index";

    let inputX = document.createElement('input');
    inputX.name = "x";
    inputX.hidden = "true";
    inputX.value = x;
    form.appendChild(inputX);

    let inputY = document.createElement('input');
    inputY.name = "y";
    inputY.hidden = "true";
    inputY.value = y;
    form.appendChild(inputY);

    let inputR = document.createElement('input');
    inputR.name = "r";
    inputR.hidden = "true";
    inputR.value = r;
    form.appendChild(inputR);

    document.body.appendChild(form);
    form.submit();
}
